<?php 
require_once("fonctions-res.php");
global $auteur;
$auteur = retourn_auteur();
if (isset($_POST["action"])) {
    if ($_POST["action"] == "Poster") {
        echo "no";
        if (empty($_FILES['post_img']['name'])) {
            if (!empty($_POST['post_msg']))
                ajoute_post_msg();
        } else
            ajoute_post_img();
    } else {
        $action = explode("_", $_POST["action"]);
        if ($action[0] == "Aimer")
            aimer($action[1]);
        if ($action[0] == "Commenter" && !empty($_POST["post_com_".$action[1]]))
            ajoute_post_com();
    }
}
?>

<html> 
	<head> 
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="style-res.css">
		<title>
			reseau social
		</title>                        
	</head> 
	<body>
        <form action="res.php" method="post" enctype="multipart/form-data">
            <div class="menu">
                <input type="checkbox" name="choix_img" value="images" <?php choix_courant("images");?>>Images
                <input type="checkbox" name="choix_msg" value="messages" <?php choix_courant("messages");?>>Messages
                <input type="radio" name="choix_com" value="Avec commentaires" <?php choix_courant("avec commentaires");?>>Avec commentaires
                <input type="radio" name="choix_com" value="Sans commentaires" <?php choix_courant("sans commentaires");?>>Sans commentaires
                Nombres de posts?  
                <select name="choix_nbp">
                    <option value="1" <?php choix_courant("1");?>>1</option>
                    <option value="5" <?php choix_courant("5");?>>5</option>
                    <option value="10" <?php choix_courant("10");?>>10</option>
                    <option value="25" <?php choix_courant("25");?>>25</option>
                    <option value="50" <?php choix_courant("50");?>>50</option>
                </select>
                <button name="action" value="Afficher">Afficher
            </div> 
            <div class="contenu">
                <b>Bonjour <?php echo $auteur;?></b>
                <div class="nouveau_post">
                    <textarea class="text_area" name="post_msg" rows="4">Quoi de neuf?</textarea>
                    <!-- <input class="text_area" name="post_msg" type="text" placeholder="enter something" /> -->
                    <p>Ajouter une image : <input type="file" name="post_img"/></p>
                    <input type="submit" name="action" value="Poster"/>
                    <input type="hidden" name="MAX_FILE_SIZE" value="500000">
                </div>
                <div class="affiche_post">
                    <!-- <div class="publication">
                        <?php affiche_post(retourne_dernier_post());?>
                    </div> -->
                    <?php 
                    if (isset($_POST['choix_msg'])) {
                        $choix = 0;
                        if (isset($_POST['choix_img']))
                            $choix = 2;
                    } elseif (isset($_POST['choix_img']))
                        $choix = 1;
                    foreach (retourne_tab_50derniersposts($choix) as $fichier) { ?>
                        <div class="publication">
                            <?php
                            $data = file(DATA."/$fichier");
                            $post = explode("." , $fichier)[0];
                            
                            echo $data[1]?><br/>
                            <?php
                            echo $data[2]?><br/>
                            <?php
                            if ($data[0] == "image\n") {?>
                                <img src="<?php echo DATA."/IMG/".$data[5];?>"><br/>
                                <?php echo $data[6];
                            } else
                                echo $data[5];
                            ?>
                            <br/>
                            <div class="comment_area">
                                <?php
                                if (isset($_POST['choix_com']) and $_POST['choix_com']=='Avec commentaires') {
                                    foreach (retourne_tab_coms($fichier) as $fichier_com){
                                        $data_com = file(DATA."/$fichier_com");
                                        $com_id = explode(".",$fichier_com)[0];?>
                                        <div class="commentaire">
                                            <?php echo $data_com[1];?>
                                            <?php echo $data_com[2];?>
                                            <?php echo $data_com[6];?>
                                            <button class="like" type="submit" name="action" value="Aimer_<?php echo $com_id;?>"><div></button>
                                            <div class="nbLike"><?php echo $data_com[3];?></div>
                                        </div>
                                <?php } } ?>
                            </div>
                            <div class="post_comment">
                            <button class="like" type="submit" name="action" value="Aimer_<?php echo $post;?>"><div></button>
                            <div class="nbLike"><?php echo $data[3];?></div>
                            <button type="submit" name="action" value="Commenter_<?php echo $post;?>">commenter</button>
                            <!-- <input class="text_area" name="post_com" type="text"/> -->
                            <textarea class="text_area" name="post_com_<?php echo $post;?>" class="commentaire" rows="1"></textarea> 
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </form>
    </body>
    <script type="text/javascript" src="live_reload.js"></script>
    <script src="https://rawgit.com/jackmoore/autosize/master/dist/autosize.min.js"></script>
    <script type="text/javascript">
        autosize(document.getElementsByClassName("text_area"));
    </script>
</html>
