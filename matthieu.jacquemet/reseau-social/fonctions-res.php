<?php
require_once("../../LIB/librairie-res.php");
define("DATA","../../DATA");

function retourn_auteur() {
    $uri = explode("/", $_SERVER["REQUEST_URI"]);
    // if (sizeof($uri) > 1)
        return $uri[1];
}

function ajoute_post_msg(){
    global $auteur;
    // if ($_POST["action"] == "Poster" && sizeof($_POST["post_msg"])) {
    $nbPost = prendre_un_post();
    $fichier = DATA."/$nbPost-msg.txt";
    $content = implode("\n", ["message", $auteur, date("Y-m-d H:i:s"),
                        "0", "", $_POST["post_msg"]]);
    file_put_contents($fichier, $content, LOCK_EX);
}

function ajoute_post_img(){
    global $auteur;
    $nbPost = prendre_un_post();
    $fichier = $nbPost."-".$_FILES['post_img']['name'];

    if (!verifie_image($fichier))
        echo "L'image reçu est invalide";
    
    $content = implode("\n", ["image", $auteur, date("Y-m-d H:i:s"),
                        "0", "", $fichier, $_POST["post_msg"]]);
    file_put_contents(DATA."/$nbPost-img.txt", $content, LOCK_EX);
}

function affiche_post($fichier) {
    $data = file(DATA."/$fichier");
    $post = explode("." , $fichier)[0];

    echo $data[1]."<br/>";
    echo $data[2]."<br/>";

    if ($data[0] == "image\n") {
        echo "<img src=\"".DATA."/IMG/".$data[5]."\"/></br>";
        echo $data[6];
    } else
        echo $data[5];

    if (isset($_POST['choix_com']) and $_POST['choix_com']=='Avec commentaires') {
        foreach (retourne_tab_coms($fichier) as $fichier_com){
            $data_com = file(DATA."/$fichier_com");
            $com_id = explode(".",$fichier_com)[0];
            echo "<div class=\"commentaire\">";
            echo "<button type=\"submit\" name=\"action\" value=\"Aimer_$com_id\">Aimer</button>";
            echo "<div class=\"nbLike\">".$data_com[3]."</div>";
            echo $data_com[6];
            echo "</div>";
        }
    }

    echo "<button type=\"submit\" name=\"action\" value=\"Aimer_$post\"/>aimer</button>";
    echo "<div class=\"nbLike\">".$data[3]."</div>";
    echo "<button type=\"submit\" name=\"action\" value=\"Commenter_$post\">commenter</button>";
    echo "<textarea name=\"post_com_$post\" class=\"commentaire\" rows=\"1\"></textarea>";    

}

function ajoute_post_com() {
    global $auteur;
    $nbPost = prendre_un_post();

    $id = explode("_", $_POST["action"])[1];
    $content = implode("\n", ["commentaire", $auteur, date("Y-m-d H:i:s"),
                        "0", "", "./$id.txt", $_POST["post_com_$id"]]);

    file_put_contents(DATA."/$nbPost-com.txt", $content, LOCK_EX);
}

?>